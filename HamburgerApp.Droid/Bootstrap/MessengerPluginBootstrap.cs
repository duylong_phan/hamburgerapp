using MvvmCross.Platform.Plugins;

namespace HamburgerApp.Droid.Bootstrap
{
    public class MessengerPluginBootstrap
        : MvxPluginBootstrapAction<MvvmCross.Plugins.Messenger.PluginLoader>
    {
    }
}