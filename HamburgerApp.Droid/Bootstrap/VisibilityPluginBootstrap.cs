using MvvmCross.Platform.Plugins;

namespace HamburgerApp.Droid.Bootstrap
{
    public class VisibilityPluginBootstrap
        : MvxPluginBootstrapAction<MvvmCross.Plugins.Visibility.PluginLoader>
    {
    }
}