using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using MvvmCross.Droid.Shared.Presenter;
using System.Reflection;
using MvvmCross.Core.ViewModels;
using HamburgerApp.Core.Models.Data;
using MvvmCross.Platform;
using HamburgerApp.Core.Interfaces.Services;
using MvvmCross.Platform.Droid.Platform;

namespace HamburgerApp.Droid.Models.Data
{
    public class HarmburgerViewPresenter : MvxFragmentsPresenter
    {
        public HarmburgerViewPresenter(IEnumerable<Assembly> androidViewAssemblies)
            : base(androidViewAssemblies)
        {

        }

        public override void ChangePresentation(MvxPresentationHint hint)
        {
            var item = hint as MvxPanelActionPresentationHint;
            var service = Mvx.GetSingleton<IChangeViewWithHintService>();
            if (item != null && service != null)
            {
                service.ChangeViewWithHintService(Mvx.Resolve<IMvxAndroidCurrentTopActivity>().Activity, item.Hint);
            }

            base.ChangePresentation(hint);
        }
    }
}