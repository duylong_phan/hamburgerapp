using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using MvvmCross.Droid.Shared.Attributes;
using HamburgerApp.Core.ViewModels;
using HamburgerApp.Core.ViewModels.Contents;
using HamburgerApp.Droid.Views.Base;

namespace HamburgerApp.Droid.Views.Contents
{
    [MvxFragment(typeof(MainViewModel), Resource.Id.content_frame, true)]
    [Register("hamburgerapp.droid.views.contents.HomeAction2View")]
    class HomeAction2View : BaseFragment<HomeAction2ViewModel>
    {
        public HomeAction2View()
            : base(false, Resource.Layout.fragment_homeaction2view)
        {

        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            //Do Something When View is created
            return base.OnCreateView(inflater, container, savedInstanceState);
        }
    }
}