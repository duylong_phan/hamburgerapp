using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using HamburgerApp.Droid.Views.Base;
using HamburgerApp.Core.ViewModels.Contents;
using MvvmCross.Droid.Shared.Attributes;
using HamburgerApp.Core.ViewModels;

namespace HamburgerApp.Droid.Views.Contents
{
    [MvxFragment(typeof(MainViewModel), Resource.Id.content_frame)]
    [Register("hamburgerapp.droid.views.contents.HomeView")]
    public class HomeView : BaseFragment<HomeViewModel>
    {
        public HomeView()
            : base(true, Resource.Layout.fragment_homeView)
        {

        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            //Do Something When View is created            
            return base.OnCreateView(inflater, container, savedInstanceState);
        }
    }
}