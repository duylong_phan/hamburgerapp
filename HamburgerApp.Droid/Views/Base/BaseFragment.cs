using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using MvvmCross.Droid.Support.V4;
using MvvmCross.Core.ViewModels;
using MvvmCross.Binding.Droid.BindingContext;
using Android.Support.V7.Widget;
using MvvmCross.Droid.Support.V7.AppCompat;
using Android.Content.Res;
using Android.Support.V4.Widget;
using HamburgerApp.Core.ViewModels.Base;
using MvvmCross.Platform;
using LongPortableLibrary.Interfaces.Models.Services;

namespace HamburgerApp.Droid.Views.Base
{
    public abstract class BaseFragment: MvxFragment
    {
        #region Getter
        public bool IsTopFragment { get; private set; }
        #endregion

        #region Field
        private int _fragmentId;
        private Toolbar _toolbar;
        private MvxActionBarDrawerToggle _menuButton;
        #endregion

        #region Constructor
        public BaseFragment(bool showHamburgerButton, int fragmentId)
        {
            this.RetainInstance = true;
            this.IsTopFragment = showHamburgerButton;
            _fragmentId = fragmentId;
        }
        #endregion

        #region Override Method  
        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            base.OnCreateView(inflater, container, savedInstanceState);
            //Notify
            var service = Mvx.Resolve<INotifyViewChangedService>();
            service.RequestViewChangedEvent(this);

            var mainView = this.Activity as MainView;
            var view = this.BindingInflate(_fragmentId, null);                        
            this._toolbar = view.FindViewById<Toolbar>(Resource.Id.toolbar);
            if (_toolbar != null && mainView != null)
            {
                //Set ToolBar in place of actionBar
                mainView.SetSupportActionBar(_toolbar);
                //En/disable gesture of drawer
                mainView.EnableHamburgerMenuGesture(this.IsTopFragment);        
                //Show Icon on left of toolbar
                mainView.SupportActionBar.SetDisplayHomeAsUpEnabled(true);
                //enable Home Button ???
                //mainView.SupportActionBar.SetDisplayShowHomeEnabled(true);

                //Home Icon will be replace by Hamburger Icon
                if (this.IsTopFragment)
                {
                    this._menuButton = new MvxActionBarDrawerToggle(mainView, mainView.DrawerLayout,
                                                                   Resource.String.drawer_opened, Resource.String.drawer_closed);
                    //Event
                    this._menuButton.DrawerOpened += (_sender, _e) =>
                    {
                        mainView.HideSoftKeyboard();
                    };
                    this._menuButton.DrawerClosed += (_sender, _e) =>
                    {

                    };
                    //Subscribe
                    mainView.DrawerLayout.AddDrawerListener(this._menuButton);                    
                }
            }

            return view;
        }

        public override void OnConfigurationChanged(Configuration newConfig)
        {
            base.OnConfigurationChanged(newConfig);
            if (this._toolbar != null && this._menuButton != null)
            {
                this._menuButton.OnConfigurationChanged(newConfig);
            }
        }

        public override void OnActivityCreated(Bundle savedInstanceState)
        {
            base.OnActivityCreated(savedInstanceState);
            if (this._toolbar != null && this._menuButton != null)
            {
                this._menuButton.SyncState();
            }
        }
        #endregion
    }

    public abstract class BaseFragment<TViewModel> : BaseFragment
        where TViewModel : class, IMvxViewModel
    {
        // Important => Override Base Class Property => Identify ViewModel for Fragment
        public new TViewModel ViewModel
        {
            get { return (TViewModel)base.ViewModel; }
            set { base.ViewModel = value; }
        }

        public BaseFragment(bool showHamburgerButton, int fragmentId)
            : base(showHamburgerButton, fragmentId)
        {

        }
    }
}