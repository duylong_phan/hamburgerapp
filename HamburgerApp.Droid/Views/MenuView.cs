using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using MvvmCross.Droid.Support.V4;
using HamburgerApp.Core.ViewModels;
using Android.Support.Design.Widget;
using MvvmCross.Droid.Shared.Attributes;
using MvvmCross.Binding.Droid.BindingContext;
using System.Threading.Tasks;

namespace HamburgerApp.Droid.Views
{
    [MvxFragment(typeof(MainViewModel), Resource.Id.navigation_frame)]
    [Register("hamburgerapp.droid.views.MenuView")]
    public class MenuView : MvxFragment<MenuViewModel>, NavigationView.IOnNavigationItemSelectedListener
    {
        #region Override Method
        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            base.OnCreateView(inflater, container, savedInstanceState);

            var view = this.BindingInflate(Resource.Layout.fragment_menu, null);            
            var navigationView = view.FindViewById<NavigationView>(Resource.Id.navigation_view);
            //disable Material design on Navigation View Icon
            //_navigationView.ItemIconTintList = null;

            //Subscribe Event
            navigationView.SetNavigationItemSelectedListener(this);

            //select Home Option
            navigationView.Menu.FindItem(Resource.Id.nav_home).SetChecked(true);

            return view;
        }

        public bool OnNavigationItemSelected(IMenuItem item)
        {
            var mainView = this.Activity as MainView;
            if (mainView != null)
            {
                mainView.HideHamburgerMenuContent();
            }

            ExecuteMenuCommand(item.ItemId);            
            return true;
        }
        #endregion

        #region Private Method
        private async void ExecuteMenuCommand(int id)
        {
            //UI Progress Action
            await Task.Delay(250);

            //Do Action
            switch (id)
            {
                case Resource.Id.nav_home:          this.ViewModel.ShowHome.TryToExecute();         break;
                case Resource.Id.nav_settings:      this.ViewModel.ShowSetting.TryToExecute();      break;
                case Resource.Id.nav_helpfeedback:  this.ViewModel.ShowHelp.TryToExecute();         break;
                default:
                    break;
            }
        }
        #endregion
    }
}