using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Content.PM;
using HamburgerApp.Core.ViewModels;
using MvvmCross.Droid.Support.V7.AppCompat;
using Android.Support.V4.Widget;
using Android.Views.InputMethods;
using Android.Support.V4.View;
using HamburgerApp.Droid.Views.Contents;
using MvvmCross.Platform;
using MvvmCross.Droid.Support.V4;
using HamburgerApp.Droid.Views.Base;
using LongPortableLibrary.Interfaces.Models.Services;
using HamburgerApp.Core.ViewModels.Base;
using Android.Support.V7.Widget;

namespace HamburgerApp.Droid.Views
{
    [Activity(
         Label = "Main Activity",
         Theme = "@style/AppTheme",
         LaunchMode = LaunchMode.SingleTop,
         Name = "hamburgerapp.droid.views.MainView"
     )]
    public class MainView : MvxCachingFragmentCompatActivity<MainViewModel>
    {
        #region Static
        public static BaseFragment CurrentView { get; private set; }
        #endregion

        #region Getter
        public DrawerLayout DrawerLayout { get; private set; }        
        #endregion

        #region Override Method
        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);
            SetContentView(Resource.Layout.activity_mainView);

            //Get Drawer to later purpose
            this.DrawerLayout = FindViewById<DrawerLayout>(Resource.Id.drawer_layout);

            //no idea
            if (bundle == null)
            {
                this.ViewModel.ShowMenu();
            }

            //Subscribe Service
            var service = Mvx.Resolve<INotifyViewChangedService>();
            service.ViewChanged += Service_ViewChanged;
        }        

        //Trigger to implement Toolbar: load new Activity, or load new Fragment
        public override bool OnCreateOptionsMenu(IMenu menu)
        {
            var isHandled = false;
            //Use CurrentView to device which menu is implementied          
            if (MainView.CurrentView is HomeView)
            {
                MenuInflater.Inflate(Resource.Menu.menu_homeMenu, menu);
                isHandled = true;
            }
            else if (MainView.CurrentView is HomeAction1View)
            {
                MenuInflater.Inflate(Resource.Menu.menu_homeAction1, menu);
                isHandled = true;
            }
            else if (MainView.CurrentView is HomeAction2View)
            {
                MenuInflater.Inflate(Resource.Menu.menu_homeAction2, menu);
                isHandled = true;
            }
            //Important => always available => initialize EventHandler in Base Class
            else
            {                
                isHandled = base.OnCreateOptionsMenu(menu);
            }

            return isHandled;
        }

        //Trigger when an item on Toolbar is clicked
        public override bool OnOptionsItemSelected(IMenuItem item)
        {
            var isHandled = true;
            switch (item.ItemId)
            {
                //Top, Left Button on Toolbar is pressed
                case Android.Resource.Id.Home:
                    //As Hamburger Button
                    if (MainView.CurrentView != null && MainView.CurrentView.IsTopFragment)
                    {
                        ShowHamburgerMenuContent();
                    }
                    //As Home Button
                    else if(this.SupportFragmentManager.BackStackEntryCount > 0)
                    {
                        this.SupportFragmentManager.PopBackStack();
                    }
                    //Undefined
                    else
                    {
                        isHandled = false;
                    }  
                    break;

                    //Event is not handled
                default:
                    isHandled = false;
                    break;
            }

            return isHandled;
        }

        //Trigger when Back Button is clicked
        public override void OnBackPressed()
        {
            //Close Hamburger Menu if it is visible
            if (this.DrawerLayout != null &&
                this.DrawerLayout.IsDrawerOpen(GravityCompat.Start))
            {
                HideHamburgerMenuContent();
            }
            //do as normal
            else
            {
                base.OnBackPressed();
            }
        }
        
        #endregion

        #region Public Method
        public void HideSoftKeyboard()
        {
            var inputMethodManager = GetSystemService(Context.InputMethodService) as InputMethodManager;
            if (this.CurrentFocus != null && inputMethodManager != null)
            {
                inputMethodManager.HideSoftInputFromWindow(this.CurrentFocus.WindowToken, HideSoftInputFlags.None);
                this.CurrentFocus.ClearFocus();
            }
        }

        public void ShowHamburgerMenuContent()
        {
            //always with GravityCompat.Start
            this.DrawerLayout.OpenDrawer(GravityCompat.Start);
        }

        public void HideHamburgerMenuContent()
        {
            //force to close
            this.DrawerLayout.CloseDrawers();
        }

        public void EnableHamburgerMenuGesture(bool isEnabled)
        {
            //Allow Gesture to show Hamburger menu
            if (isEnabled)
            {
                this.DrawerLayout.SetDrawerLockMode(DrawerLayout.LockModeUnlocked);
            }
            //Closed and disable Gesture
            else
            {
                this.DrawerLayout.SetDrawerLockMode(DrawerLayout.LockModeLockedClosed);
            }
        }
        #endregion

        #region Event Handler
        private void Service_ViewChanged(object sender, object e)
        {
            MainView.CurrentView = e as BaseFragment;
            if (MainView.CurrentView == null )
            {
                return;
            }

            //Set Properties
            var viewModel = MainView.CurrentView.ViewModel as ContentViewModelBase;
            if (viewModel != null)
            {
                //Update Title
                this.Title = viewModel.Title;                
            }
        }
        #endregion
    }
}