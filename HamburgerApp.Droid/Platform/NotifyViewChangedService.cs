using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using LongPortableLibrary.Interfaces.Models.Services;

namespace HamburgerApp.Droid.Platform
{
    class NotifyViewChangedService : INotifyViewChangedService
    {
        public event EventHandler<object> ViewChanged;
        protected void OnViewChanged(object view)
        {
            var handler = ViewChanged;
            if (handler != null)
            {
                handler(this, view);
            }
        }

        public void RequestViewChangedEvent(object view)
        {
            OnViewChanged(view);
        }
    }
}