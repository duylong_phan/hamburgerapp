using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using HamburgerApp.Core.Interfaces.Services;
using HamburgerApp.Core.Models.Data;

namespace HamburgerApp.Droid.Platform
{
    public class ChangeViewWithHintWorker : IChangeViewWithHintService
    {
        public bool ChangeViewWithHintService(object presenter, ActionPresentationHints hint)
        {
            var isHandled = false;
            var view = presenter as Android.Support.V4.App.FragmentActivity;
            if (view != null)
            {
                switch (hint)
                {
                    case ActionPresentationHints.PopToRoot:
                        if (view.SupportFragmentManager != null)
                        {
                            for (int i = 0; i < view.SupportFragmentManager.BackStackEntryCount; i++)
                            {
                                view.SupportFragmentManager.PopBackStack();
                            }
                        }
                        isHandled = true;
                        break;

                    case ActionPresentationHints.Non:
                    default:
                        break;
                }
            }
            return isHandled;
        }
    }
}