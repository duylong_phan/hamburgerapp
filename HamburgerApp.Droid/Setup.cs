using Android.Content;
using MvvmCross.Droid.Platform;
using MvvmCross.Core.ViewModels;
using MvvmCross.Platform.Platform;
using MvvmCross.Platform;
using HamburgerApp.Core.Interfaces.Services;
using HamburgerApp.Droid.Platform;
using MvvmCross.Droid.Views;
using MvvmCross.Droid.Shared.Presenter;
using HamburgerApp.Core.Models.Data;
using System.Collections.Generic;
using System.Reflection;
using MvvmCross.Platform.Droid.Platform;
using HamburgerApp.Droid.Models.Data;
using LongPortableLibrary.Interfaces.Models.Services;

namespace HamburgerApp.Droid
{
    public class Setup : MvxAndroidSetup
    {
        public Setup(Context applicationContext) : base(applicationContext)
        {
        }

        #region Override Method
        protected override IMvxApplication CreateApp()
        {
            return new Core.App();
        }

        protected override IMvxTrace CreateDebugTrace()
        {
            return new DebugTrace();
        }

        protected override void InitializeIoC()
        {
            base.InitializeIoC();

            Mvx.RegisterSingleton<ILogInService>(() => new AppLoginService());
            Mvx.RegisterSingleton<IChangeViewWithHintService>(() => new ChangeViewWithHintWorker());
            Mvx.RegisterSingleton<INotifyViewChangedService>(() => new NotifyViewChangedService());
        }

        protected override IEnumerable<Assembly> AndroidViewAssemblies
        {
            get
            {
                return new List<Assembly>(base.AndroidViewAssemblies)
                {
                    typeof(Android.Support.Design.Widget.NavigationView).Assembly,
                    typeof(Android.Support.Design.Widget.FloatingActionButton).Assembly,
                    typeof(Android.Support.V7.Widget.Toolbar).Assembly,
                    typeof(Android.Support.V4.Widget.DrawerLayout).Assembly,
                    typeof(Android.Support.V4.View.ViewPager).Assembly,
                    typeof(MvvmCross.Droid.Support.V7.RecyclerView.MvxRecyclerView).Assembly
                };
            }
        }

        protected override IMvxAndroidViewPresenter CreateViewPresenter()
        {
            return new HarmburgerViewPresenter(AndroidViewAssemblies);
        }
        #endregion
    }
}
