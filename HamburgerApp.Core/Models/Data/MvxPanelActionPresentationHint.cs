﻿using MvvmCross.Core.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HamburgerApp.Core.Models.Data
{
    public enum ActionPresentationHints
    {
        Non,
        PopToRoot
    }

    public class MvxPanelActionPresentationHint : MvxPresentationHint
    {
        #region Getter
        public ActionPresentationHints Hint { get; private set; }
        #endregion

        #region Constructor
        public MvxPanelActionPresentationHint(ActionPresentationHints hint)
        {
            this.Hint = hint;
        }
        #endregion
    }
}
