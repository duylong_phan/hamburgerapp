﻿using HamburgerApp.Core.Models.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HamburgerApp.Core.Interfaces.Services
{
    public interface IChangeViewWithHintService
    {
        bool ChangeViewWithHintService(object presenter, ActionPresentationHints hint);
    }
}
