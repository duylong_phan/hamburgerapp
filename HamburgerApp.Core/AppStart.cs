﻿using HamburgerApp.Core.Interfaces.Services;
using HamburgerApp.Core.ViewModels;
using MvvmCross.Core.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HamburgerApp.Core
{
    public class AppStart : MvxNavigatingObject, IMvxAppStart
    {
        private readonly ILogInService _loginService;

        public AppStart(ILogInService loginService)
        {
            _loginService = loginService;
        }

        public void Start(object hint = null)
        {
            if (_loginService.Login())
            {
                ShowViewModel<MainViewModel>();                
            }
            else
            {
                ShowViewModel<RootViewModel>();
            }
        }
    }
}
