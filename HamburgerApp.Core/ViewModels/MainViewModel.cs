﻿using HamburgerApp.Core.ViewModels.Contents;
using MvvmCross.Core.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HamburgerApp.Core.ViewModels
{
    public class MainViewModel : MvxViewModel
    {
        public MainViewModel()
        {

        }

        public async void ShowMenu()
        {
            ShowViewModel<MenuViewModel>();
            await Task.Delay(50);
            ShowViewModel<HomeViewModel>();            
        }
    }
}
