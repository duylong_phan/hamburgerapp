﻿using LongPortableLibrary.Models.Commands;
using MvvmCross.Core.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HamburgerApp.Core.ViewModels
{
    public class RootViewModel : MvxViewModel
    {
        #region Getter, Setter
        private string _userName;
        private string _password;

        public string UserName
        {
            get { return this._userName; }
            set
            {
                this.SetProperty(ref this._userName, value);
            }
        }        
        public string Password
        {
            get { return this._password; }
            set
            {
                this.SetProperty(ref this._password, value);
            }
        }
        #endregion

        #region Getter
        public RelayCommand LogIn { get; private set; }
        #endregion

        #region Constructor
        public RootViewModel()
        {
            this.UserName = "Long";
            this.Password = "no pass";
            this.LogIn = new RelayCommand(Exe_LogIn, Can_LogIn);
        }
        #endregion

        #region Private Method
        private bool Can_LogIn()
        {
            return true;
        }

        private void Exe_LogIn()
        {
            ShowViewModel<MainViewModel>();
        }
        #endregion
    }
}
