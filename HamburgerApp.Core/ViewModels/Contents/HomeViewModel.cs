﻿using HamburgerApp.Core.ViewModels.Base;
using LongPortableLibrary.Models.Commands;
using MvvmCross.Platform;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HamburgerApp.Core.ViewModels.Contents
{
    public class HomeViewModel : ContentViewModelBase
    {
        #region Getter
        public RelayCommand DoAction { get; private set; }
        #endregion

        #region Constructor
        public HomeViewModel()
            : base("Home")
        {
            this.DoAction = new RelayCommand(Exe_DoAction, Can_DoAction);            
        }
        #endregion

        #region Private Method
        private bool Can_DoAction()
        {
            return true;
        }

        private void Exe_DoAction()
        {            
            ShowViewModel<HomeAction1ViewModel>();
        }
        #endregion
    }
}
