﻿using HamburgerApp.Core.ViewModels.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HamburgerApp.Core.ViewModels.Contents
{
    public class SettingViewModel : ContentViewModelBase
    {
        public SettingViewModel()
            : base("Setting")
        {

        }
    }
}
