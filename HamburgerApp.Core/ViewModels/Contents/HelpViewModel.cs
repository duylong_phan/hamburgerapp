﻿using HamburgerApp.Core.ViewModels.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HamburgerApp.Core.ViewModels.Contents
{
    public class HelpViewModel : ContentViewModelBase
    {
        public HelpViewModel()
            : base("Help")
        {

        }
    }
}
