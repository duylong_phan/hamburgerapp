﻿using HamburgerApp.Core.ViewModels.Base;
using LongPortableLibrary.Models.Commands;
using MvvmCross.Platform;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HamburgerApp.Core.ViewModels.Contents
{
    public class HomeAction1ViewModel : ContentViewModelBase
    {
        #region Getter
        public RelayCommand DoAction { get; private set; }
        #endregion

        #region Constructor
        public HomeAction1ViewModel()
            : base("Home Action 1")
        {
            this.DoAction = new RelayCommand(Exe_DoAction, Can_DoAction);            
        }
        #endregion

        #region DoAction
        private bool Can_DoAction()
        {
            return true;
        }

        private void Exe_DoAction()
        {
            ShowViewModel<HomeAction2ViewModel>();
        }
        #endregion
    }
}
