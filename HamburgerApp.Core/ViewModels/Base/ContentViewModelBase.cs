﻿using MvvmCross.Core.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HamburgerApp.Core.ViewModels.Base
{
    public class ContentViewModelBase : MvxViewModel
    {
        public string Title { get; private set; }

        public ContentViewModelBase(string title)            
        {
            this.Title = title;
        }
    }
}
