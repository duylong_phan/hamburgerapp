﻿using LongPortableLibrary.Models.Commands;
using MvvmCross.Core.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HamburgerApp.Core.ViewModels.Contents;

namespace HamburgerApp.Core.ViewModels
{
    public class MenuViewModel : MvxViewModel
    {
        
        public RelayCommand ShowHome { get; private set; }
        public RelayCommand ShowSetting { get; private set; }
        public RelayCommand ShowHelp { get; private set; }

        

        public MenuViewModel()
        {
            this.ShowHome = new RelayCommand(Exe_ShowHome, Can_ShowHome);
            this.ShowSetting = new RelayCommand(Exe_ShowSetting, Can_ShowSetting);
            this.ShowHelp = new RelayCommand(Exe_ShowHelp, Can_ShowHelp);
        }

        #region Private Method
        private bool Can_ShowHelp()
        {
            return true;
        }

        private void Exe_ShowHelp()
        {
            ShowViewModel<HelpViewModel>();
        }
        
        private bool Can_ShowSetting()
        {
            return true;
        }

        private void Exe_ShowSetting()
        {
            ShowViewModel<SettingViewModel>();
        }
        
        private bool Can_ShowHome()
        {
            return true;
        }

        private void Exe_ShowHome()
        {
            ShowViewModel<HomeViewModel>();
        }
        #endregion
    }
}
