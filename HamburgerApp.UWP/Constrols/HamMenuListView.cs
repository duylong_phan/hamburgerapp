﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Media;

namespace HamburgerApp.UWP.Constrols
{
    public class HamMenuListView : ListView
    {
        #region Event
        public event EventHandler<object> MenuItemSelected;

        protected void OnMenuItemSelected(object item)
        {
            var handler = MenuItemSelected;
            if (handler != null)
            {
                handler(this, item);
            }
        }
        #endregion

        #region Field
        private SplitView _splitViewHost;
        #endregion

        #region Constructor
        public HamMenuListView()
        {
            //Enable Single Selection
            SelectionMode = ListViewSelectionMode.Single;

            //enable ItemClick Event
            IsItemClickEnabled = true;
            ItemClick += NavMenuListView_ItemClick;

            this.Loaded += NavMenuListView_Loaded;
        }
        #endregion

        #region Event handler
        private void NavMenuListView_Loaded(object sender, RoutedEventArgs e)
        {
            this.Loaded -= NavMenuListView_Loaded;

            //find parent as SplitView
            var parent = VisualTreeHelper.GetParent(this);
            while (parent != null && !(parent is SplitView))
            {
                parent = VisualTreeHelper.GetParent(parent);
            }

            if (parent != null)
            {
                //Assign, Register Event
                _splitViewHost = parent as SplitView;
                _splitViewHost.RegisterPropertyChangedCallback(SplitView.IsPaneOpenProperty, OnPaneOpenChanged);

                // Call once to ensure we're in the correct state
                OnPaneToggled();
            }
        }

        private void OnPaneOpenChanged(DependencyObject sender, DependencyProperty dp)
        {
            OnPaneToggled();
        }

        private void NavMenuListView_ItemClick(object sender, ItemClickEventArgs e)
        {
            OnMenuItemSelected(e.ClickedItem);

            //Close SplitView
            if (_splitViewHost.IsPaneOpen && (
                _splitViewHost.DisplayMode == SplitViewDisplayMode.CompactOverlay ||
                _splitViewHost.DisplayMode == SplitViewDisplayMode.Overlay))
            {
                _splitViewHost.IsPaneOpen = false;
            }
        }

        private void OnPaneToggled()
        {
            //Currently open
            if (_splitViewHost.IsPaneOpen)
            {
                //Clear Width => Default: Auto
                ItemsPanelRoot.ClearValue(FrameworkElement.WidthProperty);
                //Clear HorizontalAlignment => Default: Stretch
                ItemsPanelRoot.ClearValue(FrameworkElement.HorizontalAlignmentProperty);
            }
            //Currently close
            else if (_splitViewHost.DisplayMode == SplitViewDisplayMode.CompactInline ||
                     _splitViewHost.DisplayMode == SplitViewDisplayMode.CompactOverlay)
            {
                ItemsPanelRoot.Width = this._splitViewHost.CompactPaneLength;
                ItemsPanelRoot.HorizontalAlignment = HorizontalAlignment.Left;
            }
        }
        #endregion
    }
}
