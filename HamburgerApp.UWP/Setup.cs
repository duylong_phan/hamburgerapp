﻿using HamburgerApp.Core.Interfaces.Services;
using HamburgerApp.UWP.Models.Data;
using HamburgerApp.UWP.Platform;
using LongPortableLibrary.Interfaces.Models.Services;
using MvvmCross.Core.ViewModels;
using MvvmCross.Platform;
using MvvmCross.WindowsUWP.Platform;
using MvvmCross.WindowsUWP.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml.Controls;

namespace HamburgerApp.UWP
{
    public class Setup : MvxWindowsSetup
    {
        public Setup(Frame rootFrame) : base(rootFrame)
        {
        }

        protected override IMvxApplication CreateApp()
        {
            return new Core.App();
        }
                

        protected override IMvxWindowsViewPresenter CreateViewPresenter(IMvxWindowsFrame rootFrame)
        {
            return new HarmburgerViewPresenter(rootFrame);
        }

        protected override void InitializeIoC()
        {
            base.InitializeIoC();

            Mvx.RegisterSingleton<IGetMenuItemsService>(() => new MenuItemCollection());
            Mvx.RegisterSingleton<INotifyViewChangedService>(() => new NotifyViewChangedService());
            Mvx.RegisterSingleton<ILogInService>(() => new AppLoginService());
            Mvx.RegisterSingleton<IChangeViewWithHintService>(() => new ChangeViewWithHintWorker());
        }
    }
}
