﻿using LongPortableLibrary.Interfaces.Models.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HamburgerApp.UWP.Platform
{
    public class NotifyViewChangedService : INotifyViewChangedService
    {
        public event EventHandler<object> ViewChanged;
        protected void OnViewChanged(object view)
        {
            var handler = ViewChanged;
            if (handler != null)
            {
                handler(this, view);
            }
        }
        
        public void RequestViewChangedEvent(object view)
        {
            OnViewChanged(view);
        }
    }
}
