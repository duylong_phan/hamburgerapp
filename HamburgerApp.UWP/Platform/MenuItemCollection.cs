﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;
using LongPortableLibrary.Models.Data;
using HamburgerApp.Core.ViewModels;
using Windows.UI.Xaml.Controls;
using LongPortableLibrary.Interfaces.Models.Services;

namespace HamburgerApp.UWP.Platform
{
    public class MenuItemCollection : IGetMenuItemsService
    {
        public IEnumerable GetMenuItems(object viewModel)
        {
            var source = viewModel as MenuViewModel;
            var collection = new List<MenuItemInfo>();
            collection.Add(new MenuItemInfo("Home", (char)Symbol.Home, source.ShowHome, null));
            collection.Add(new MenuItemInfo("Setting", (char)Symbol.Setting, source.ShowSetting, null));
            collection.Add(new MenuItemInfo("Help", (char)Symbol.Help, source.ShowHelp, null));
            return collection;
        }
    }
}
