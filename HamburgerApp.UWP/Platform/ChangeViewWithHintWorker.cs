﻿using HamburgerApp.Core.Interfaces.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HamburgerApp.Core.Models.Data;
using HamburgerApp.UWP.Views;

namespace HamburgerApp.UWP.Platform
{
    public class ChangeViewWithHintWorker : IChangeViewWithHintService
    {
        public bool ChangeViewWithHintService(object presenter, ActionPresentationHints hint)
        {
            var isHandled = false;
            var view = presenter as MainView;
            if (view != null)
            {
                switch (hint)
                {                    
                    case ActionPresentationHints.PopToRoot:
                        view.PopToRoot();
                        isHandled = true;
                        break;

                    case ActionPresentationHints.Non:                        
                    default:
                        break;
                }
            }

            return isHandled;
        }
    }
}
