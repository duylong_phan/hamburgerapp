﻿using HamburgerApp.Core.ViewModels.Base;
using LongPortableLibrary.Interfaces.Models.Services;
using MvvmCross.Platform;
using MvvmCross.WindowsUWP.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml.Navigation;

namespace HamburgerApp.UWP.Views.Base
{
    public class ContentBaseView : MvxWindowsPage
    {
        public bool IsTopView { get; private set; }

        public ContentBaseView(bool isTopView)
        {
            this.IsTopView = isTopView;
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);

            //Update Page Title
            var service = Mvx.GetSingleton<INotifyViewChangedService>();
            var source = this.ViewModel as ContentViewModelBase;
            if (service != null && source != null)
            {
                service.RequestViewChangedEvent(this);
            }
        }
    }
}
