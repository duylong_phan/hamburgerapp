﻿using HamburgerApp.Core.ViewModels;
using HamburgerApp.Core.ViewModels.Base;
using HamburgerApp.UWP.Views.Base;
using LongPortableLibrary.Interfaces.Models.Services;
using MvvmCross.Platform;
using MvvmCross.WindowsUWP.Views;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;


namespace HamburgerApp.UWP.Views
{
    public sealed partial class MainView : MvxWindowsPage
    {
        #region Static
        public static ContentBaseView CurrentView { get; private set; }
        #endregion

        #region Event
        public event EventHandler MenuOpened;
        public event EventHandler MenuClosed;

        private void OnMenuOpened(EventArgs arg)
        {
            var handler = MenuOpened;
            if (handler != null)
            {
                handler(this, arg);
            }
        }
        private void OnMenuClosed(EventArgs arg)
        {
            var handler = MenuClosed;
            if (handler != null)
            {
                handler(this, arg);
            }
        }
        #endregion

        #region Constructor
        public MainView()
        {
            this.InitializeComponent();
            this.Loaded += MainView_Loaded;
        }
        #endregion

        #region Public Method
        public void PopToRoot()
        {
            var frame = PageContent;
            while (frame.CanGoBack)
            {
                frame.GoBack();
            }
        }
        #endregion

        #region Event Handler
        private void MainView_Loaded(object sender, RoutedEventArgs e)
        {
            this.Loaded -= MainView_Loaded;
            var source = this.ViewModel as MainViewModel;
            if (source != null)
            {
                source.ShowMenu();
            }

            //Subscribe Event to notify Page's title Changed
            var service = Mvx.GetSingleton<INotifyViewChangedService>();
            if (service != null)
            {
                service.ViewChanged += Service_ViewChanged;
            }
        }

        private void Service_ViewChanged(object sender, object e)
        {
            //Update Current View
            MainView.CurrentView = e as ContentBaseView;

            if (MainView.CurrentView != null)
            {
                //Update Title
                var viewModel = MainView.CurrentView.ViewModel as ContentViewModelBase;
                if (viewModel != null)
                {
                    this.PageTitle.Text = viewModel.Title;
                }

                //always => avoid multi Click Event Handler
                this.TogglePaneButton.Click -= TogglePaneButton_Click;

                //Enable Hamburger Menu: Show Menu Icon, enable Button, and SplitView function
                if (MainView.CurrentView.IsTopView)
                {
                    this.TogglePaneButton.Content = '\uE700';
                    this.TogglePaneButton.EnableToggle = true;
                    this.MenuContent.IsEnabled = true;
                }
                //Enable Back Function: Show Back Icon, disable Button, and SplitView function
                //handle Button Event for Back Action
                else
                {
                    this.TogglePaneButton.Content = (char)Symbol.Back;
                    this.TogglePaneButton.EnableToggle = false;
                    this.MenuContent.IsEnabled = false;
                    this.TogglePaneButton.Click += TogglePaneButton_Click;
                }
            }
        }

        private void TogglePaneButton_Click(object sender, RoutedEventArgs e)
        {
            if (this.PageContent.CanGoBack)
            {
                this.PageContent.GoBack();
            }
        }

        private void Menu_Opened(object sender, RoutedEventArgs e)
        {
            OnMenuOpened(new EventArgs());
        }

        private void Menu_Closed(object sender, RoutedEventArgs e)
        {
            OnMenuClosed(new EventArgs());
        }
        #endregion
    }
}
