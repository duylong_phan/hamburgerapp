﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls.Primitives;

namespace HamburgerApp.UWP.Views.UserControls
{
    public class ExternToggleButton : ToggleButton
    {
        #region DP
        public bool EnableToggle
        {
            get { return (bool)GetValue(EnableToggleProperty); }
            set { SetValue(EnableToggleProperty, value); }
        }
                
        public static readonly DependencyProperty EnableToggleProperty =
            DependencyProperty.Register("EnableToggle", typeof(bool), typeof(ExternToggleButton), new PropertyMetadata(true));
        #endregion

        #region Private Method
        protected override void OnToggle()
        {
            if (this.EnableToggle)
            {
                base.OnToggle();
            }            
        }
        #endregion
    }
}
