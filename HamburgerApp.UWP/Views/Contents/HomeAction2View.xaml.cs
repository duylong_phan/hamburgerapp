﻿using HamburgerApp.Core.ViewModels.Base;
using HamburgerApp.UWP.Views.Base;
using MvvmCross.Platform;
using MvvmCross.WindowsUWP.Views;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

namespace HamburgerApp.UWP.Views.Contents
{
    [MvxRegion("PageContent")]
    public sealed partial class HomeAction2View : ContentBaseView
    {
        public HomeAction2View()
            : base(false)
        {
            this.InitializeComponent();
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);

            //Do some thing when page is available            
        }
    }
}
