﻿using LongPortableLibrary.Interfaces.Models.Services;
using LongPortableLibrary.Models.Data;
using MvvmCross.Platform;
using MvvmCross.WindowsUWP.Views;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

namespace HamburgerApp.UWP.Views
{
    [MvxRegion("MenuContent")]
    public sealed partial class MenuView : MvxWindowsPage
    {
        #region Constructor
        public MenuView()
        {
            this.InitializeComponent();
        }
        #endregion

        #region Private Method
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);

            //Assign List of MenuItem
            var service = Mvx.GetSingleton<IGetMenuItemsService>();
            this.menuListView.ItemsSource = service.GetMenuItems(this.ViewModel);
        }
        #endregion

        #region Event Handler
        private void menuListView_MenuItemSelected(object sender, object e)
        {
            var menuItem = e as MenuItemInfo;
            if (menuItem != null && menuItem.Command != null)
            {
                if (menuItem.Command.CanExecute(menuItem.Parameter))
                {
                    menuItem.Command.Execute(menuItem.Parameter);
                }
            }
        }
        #endregion
    }
}
