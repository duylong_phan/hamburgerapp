﻿using MvvmCross.WindowsUWP.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MvvmCross.Core.ViewModels;
using HamburgerApp.UWP.Views;
using HamburgerApp.Core.Models.Data;
using MvvmCross.Platform;
using HamburgerApp.Core.Interfaces.Services;

namespace HamburgerApp.UWP.Models.Data
{
    public class HarmburgerViewPresenter : MvxWindowsMultiRegionViewPresenter
    {
        #region Field
        private IMvxWindowsFrame _rootFrame;
        #endregion

        #region Constructor
        public HarmburgerViewPresenter(IMvxWindowsFrame rootFrame)
            : base(rootFrame)
        {
            _rootFrame = rootFrame;
        }
        #endregion

        #region 
        public override void ChangePresentation(MvxPresentationHint hint)
        {
            var item = hint as MvxPanelActionPresentationHint;
            var service = Mvx.GetSingleton<IChangeViewWithHintService>();
            if (item != null && service != null)
            {
                service.ChangeViewWithHintService(_rootFrame.Content, item.Hint);
            }

            base.ChangePresentation(hint);
        }
        #endregion
    }
}
